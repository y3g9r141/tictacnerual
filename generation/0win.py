import numpy as np
import cv2
import random
from tensorflow import keras
import json
#1 = x win
#0 = 0 win

data = {}
data['x_train']=[]
data['y_train']=[]
train_x = []
train_y = []


sv = 80
sm = [0,82]
b_size = 1

#h
for j in range(1):
    for k in range(b_size):

        img = np.zeros((243, 243, 1), np.uint8)
        img[:] = (255)

        cv2.line(img, (81, 0), (81, 243), 0, 3)
        cv2.line(img, (162, 0), (162, 243), 0, 3)

        cv2.line(img, (0, 81), (243, 81), 0, 3)
        cv2.line(img, (0, 162), (243, 162), 0, 3)

        for i in sm:
            x1 = random.randint(i+10, i+20)
            y1 = random.randint(10+sv*j, 20+sv*j)

            x2 = random.randint(i+60, i+70)
            y2 = random.randint(60+sv*j, 70+sv*j)

            cv2.line(img, (x1, y1), (x2, y2), 0, 2)

            x1 = random.randint(i+60, i+70)
            y1 = random.randint(10+sv*j, 20+sv*j)

            x2 = random.randint(i+10, i+20)
            y2 = random.randint(60+sv*j, 70+sv*j)

            cv2.line(img, (x1, y1), (x2, y2), 0, 2)

        cv2.circle(img, (40, 120), random.randint(18, 25), 0, 2)
        cv2.circle(img, (120, 120), random.randint(18, 25), 0, 2)
        cv2.circle(img, (200, 120), random.randint(18, 25), 0, 2)

        img_arr = keras.utils.img_to_array(img).tolist()
        train_x.append(img_arr)
        train_y.append(0)

        data.update({'x_train': train_x})
        data.update({'y_train': train_y})

        with open('0win.json', 'w') as datasetfile:
            json.dump(data, datasetfile)

        cv2.imshow("test",img)
        cv2.waitKey()
        cv2.imwrite("0win.png", img)