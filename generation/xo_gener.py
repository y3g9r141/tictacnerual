import numpy as np
import cv2
import random
from tensorflow import keras
import json
#1 = x win
#0 = 0 win

data = {}
data['x_train']=[]
data['y_train']=[]
train_x = []
train_y = []


sv = 80
sm = [0,82,163]
b_size = 10

#h
for j in range(3):
    for k in range(b_size):

        img = np.zeros((243, 243, 1), np.uint8)
        img[:] = (255)

        cv2.line(img, (81, 0), (81, 243), 0, 3)
        cv2.line(img, (162, 0), (162, 243), 0, 3)

        cv2.line(img, (0, 81), (243, 81), 0, 3)
        cv2.line(img, (0, 162), (243, 162), 0, 3)

        for i in sm:
            x1 = random.randint(i+10, i+20)
            y1 = random.randint(10+sv*j, 20+sv*j)

            x2 = random.randint(i+60, i+70)
            y2 = random.randint(60+sv*j, 70+sv*j)

            cv2.line(img, (x1, y1), (x2, y2), 0, 2)

            x1 = random.randint(i+60, i+70)
            y1 = random.randint(10+sv*j, 20+sv*j)

            x2 = random.randint(i+10, i+20)
            y2 = random.randint(60+sv*j, 70+sv*j)

            cv2.line(img, (x1, y1), (x2, y2), 0, 2)

        #тут производим запись генера в ф массив для файла
        img_arr = keras.utils.img_to_array(img).tolist()
        train_x.append(img_arr)
        train_y.append(1)
#v
for j in range(3):
    for k in range(b_size):

        img = np.zeros((243, 243, 1), np.uint8)
        img[:] = (255)

        cv2.line(img, (81, 0), (81, 243), 0, 3)
        cv2.line(img, (162, 0), (162, 243), 0, 3)

        cv2.line(img, (0, 81), (243, 81), 0, 3)
        cv2.line(img, (0, 162), (243, 162), 0, 3)

        for i in sm:
            x1 = random.randint(10+sv*j, 20+sv*j)
            y1 = random.randint(i+10, i+20)

            x2 = random.randint(60+sv*j, 70+sv*j)
            y2 = random.randint(i+60, i+70)

            cv2.line(img, (x1, y1), (x2, y2), 0, 2)

            x1 = random.randint(10+sv*j, 20+sv*j)
            y1 = random.randint(i+60, i+70)

            x2 = random.randint(60+sv*j, 70+sv*j)
            y2 = random.randint(i+10, i+20)

            cv2.line(img, (x1, y1), (x2, y2), 0, 2)

        #тут производим запись генера в ф массив для файла
        img_arr = keras.utils.img_to_array(img).tolist()
        train_x.append(img_arr)
        train_y.append(1)

#l1
for k in range(b_size):

    img = np.zeros((243, 243, 1), np.uint8)
    img[:] = (255)

    cv2.line(img, (81, 0), (81, 243), 0, 3)
    cv2.line(img, (162, 0), (162, 243), 0, 3)

    cv2.line(img, (0, 81), (243, 81), 0, 3)
    cv2.line(img, (0, 162), (243, 162), 0, 3)

    for i in sm:
        x1 = random.randint(i+10, i+20)
        y1 = random.randint(i+10, i+20)

        x2 = random.randint(i+60, i+70)
        y2 = random.randint(i+60, i+70)

        cv2.line(img, (x1, y1), (x2, y2), 0, 2)

        x1 = random.randint(i+60, i+70)
        y1 = random.randint(i+10, i+20)

        x2 = random.randint(i+10, i+20)
        y2 = random.randint(i+60, i+70)

        cv2.line(img, (x1, y1), (x2, y2), 0, 2)

    img_arr = keras.utils.img_to_array(img).tolist()
    train_x.append(img_arr)
    train_y.append(1)


#l2
for k in range(b_size):

    img = np.zeros((243, 243, 1), np.uint8)
    img[:] = (255)

    cv2.line(img, (81, 0), (81, 243), 0, 3)
    cv2.line(img, (162, 0), (162, 243), 0, 3)

    cv2.line(img, (0, 81), (243, 81), 0, 3)
    cv2.line(img, (0, 162), (243, 162), 0, 3)

    for i in sm:
        x1 = random.randint(170-i, 180-i)
        y1 = random.randint(i+10, i+20)

        x2 = random.randint(220-i, 230-i)
        y2 = random.randint(i+60, i+70)

        cv2.line(img, (x1, y1), (x2, y2), 0, 2)

        x1 = random.randint(220-i, 230-i)
        y2 = random.randint(i+60, i+70)

        x2 = random.randint(170-i, 180-i)
        y2 = random.randint(i+60, i+70)

        cv2.line(img, (x1, y1), (x2, y2), 0, 2)

    img_arr = keras.utils.img_to_array(img).tolist()
    train_x.append(img_arr)
    train_y.append(1)



#draw1
sv = 80
sm = [0,82,163]
b_size = 10


for t in range(10):
    img = np.zeros((243, 243, 1), np.uint8)
    img[:] = (255)

    cv2.line(img, (81, 0), (81, 243), 0, 3)
    cv2.line(img, (162, 0), (162, 243), 0, 3)

    cv2.line(img, (0, 81), (243, 81), 0, 3)
    cv2.line(img, (0, 162), (243, 162), 0, 3)
    for j in range(3):
        for k in range(1):

            for i in sm:
                if j == 0 and sm.index(i)==1:
                    continue
                if j == 1 and sm.index(i)==0:
                    continue
                if j == 1 and sm.index(i)==2:
                    continue
                if j == 2 and sm.index(i)==0:
                    continue
                if j == 2 and sm.index(i)==2:
                    continue
                x1 = random.randint(i+10, i+20)
                y1 = random.randint(10+sv*j, 20+sv*j)

                x2 = random.randint(i+60, i+70)
                y2 = random.randint(60+sv*j, 70+sv*j)

                cv2.line(img, (x1, y1), (x2, y2), 0, 2)

                x1 = random.randint(i+60, i+70)
                y1 = random.randint(10+sv*j, 20+sv*j)

                x2 = random.randint(i+10, i+20)
                y2 = random.randint(60+sv*j, 70+sv*j)

                cv2.line(img, (x1, y1), (x2, y2), 0, 2)

    cv2.circle(img, (120, 40), random.randint(18, 25), 0, 2)
    cv2.circle(img, (40, 120), random.randint(18, 25), 0, 2)
    cv2.circle(img, (40, 200), random.randint(18, 25), 0, 2)
    cv2.circle(img, (200, 120), random.randint(18, 25), 0, 2)
    cv2.circle(img, (200, 200), random.randint(18, 25), 0, 2)
    img_arr = keras.utils.img_to_array(img).tolist()
    train_x.append(img_arr)
    train_y.append(2)
#
# for t in range(10):
#     img = np.zeros((243, 243, 1), np.uint8)
#     img[:] = (255)
#
#     cv2.line(img, (81, 0), (81, 243), 0, 3)
#     cv2.line(img, (162, 0), (162, 243), 0, 3)
#
#     cv2.line(img, (0, 81), (243, 81), 0, 3)
#     cv2.line(img, (0, 162), (243, 162), 0, 3)
#     for j in range(3):
#         for k in range(1):
#
#             for i in sm:
#                 if j == 0 and sm.index(i) == 0:
#                     continue
#                 if j == 0 and sm.index(i) == 2:
#                     continue
#                 if j == 1 and sm.index(i) == 1:
#                     continue
#                 if j == 2 and sm.index(i) == 1:
#                     continue
#                 x1 = random.randint(i + 10, i + 20)
#                 y1 = random.randint(10 + sv * j, 20 + sv * j)
#
#                 x2 = random.randint(i + 60, i + 70)
#                 y2 = random.randint(60 + sv * j, 70 + sv * j)
#
#                 cv2.line(img, (x1, y1), (x2, y2), 0, 2)
#
#                 x1 = random.randint(i + 60, i + 70)
#                 y1 = random.randint(10 + sv * j, 20 + sv * j)
#
#                 x2 = random.randint(i + 10, i + 20)
#                 y2 = random.randint(60 + sv * j, 70 + sv * j)
#
#                 cv2.line(img, (x1, y1), (x2, y2), 0, 2)
#
#     cv2.circle(img, (40, 40), random.randint(18, 25), 0, 2)
#     cv2.circle(img, (200, 40), random.randint(18, 25), 0, 2)
#     cv2.circle(img, (120, 120), random.randint(18, 25), 0, 2)
#     cv2.circle(img, (120, 200), random.randint(18, 25), 0, 2)
#     img_arr = keras.utils.img_to_array(img).tolist()
#     train_x.append(img_arr)
#     train_y.append(2)








###################
#############
###############
##############
#nuliki


sv = 80
sm = [40,120,200]
b_size = 10


#h
for j in range(3):
    for k in range(b_size):

        img = np.zeros((243, 243, 1), np.uint8)
        img[:] = (255)

        cv2.line(img, (81, 0), (81, 243), 0, 3)
        cv2.line(img, (162, 0), (162, 243), 0, 3)

        cv2.line(img, (0, 81), (243, 81), 0, 3)
        cv2.line(img, (0, 162), (243, 162), 0, 3)

        for i in sm:
            cv2.circle(img,(i,sm[j]),random.randint(18,25),0,2)

        #тут производим запись генера в ф массив для файла
        img_arr = keras.utils.img_to_array(img).tolist()
        train_x.append(img_arr)
        train_y.append(0)
#v
for j in range(3):
    for k in range(b_size):

        img = np.zeros((243, 243, 1), np.uint8)
        img[:] = (255)

        cv2.line(img, (81, 0), (81, 243), 0, 3)
        cv2.line(img, (162, 0), (162, 243), 0, 3)

        cv2.line(img, (0, 81), (243, 81), 0, 3)
        cv2.line(img, (0, 162), (243, 162), 0, 3)

        for i in sm:
            cv2.circle(img,(sm[j],i),random.randint(18,25),0,2)

        #тут производим запись генера в ф массив для файла
        img_arr = keras.utils.img_to_array(img).tolist()
        train_x.append(img_arr)
        train_y.append(0)

#l1
for k in range(b_size):

    img = np.zeros((243, 243, 1), np.uint8)
    img[:] = (255)

    cv2.line(img, (81, 0), (81, 243), 0, 3)
    cv2.line(img, (162, 0), (162, 243), 0, 3)

    cv2.line(img, (0, 81), (243, 81), 0, 3)
    cv2.line(img, (0, 162), (243, 162), 0, 3)

    for i in sm:
        cv2.circle(img, (i, i), random.randint(18, 25), 0, 2)

    img_arr = keras.utils.img_to_array(img).tolist()
    train_x.append(img_arr)
    train_y.append(0)


sm = [200,120,40]
#l2
for k in range(b_size):

    img = np.zeros((243, 243, 1), np.uint8)
    img[:] = (255)

    cv2.line(img, (81, 0), (81, 243), 0, 3)
    cv2.line(img, (162, 0), (162, 243), 0, 3)

    cv2.line(img, (0, 81), (243, 81), 0, 3)
    cv2.line(img, (0, 162), (243, 162), 0, 3)

    for i in sm:
        cv2.circle(img, (i, sm[len(sm)-1-sm.index(i)]), random.randint(18, 25), 0, 2)
        cv2.line(img, (x1, y1), (x2, y2), 0, 2)

    img_arr = keras.utils.img_to_array(img).tolist()
    train_x.append(img_arr)
    train_y.append(0)






data.update({'x_train':train_x})
data.update({'y_train':train_y})

with open('dataset.json', 'w') as datasetfile:
    json.dump(data, datasetfile)













