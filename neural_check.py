import numpy as np
import cv2
import tensorflow
import json
import imageio

from matplotlib import pyplot as plt
from numpy import ndarray
from numpy import array

from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D

from keras.utils import np_utils
from keras.datasets import mnist

from tensorflow import keras



class MnistModel:
    def __init__(self):
        self.model = None

        self.X_train = None
        self.Y_train = None

        self.X_test = None
        self.Y_test = None

    def _get_train(self):
        # Random number
        np.random.seed(100)

        # Get mnist model for train
        #(X_train, y_train), (X_test, y_test) = mnist.load_data()


        with open('./samples/dataset (2).json', 'r') as f:
            map = json.load(f)

        (X_train, y_train), (X_test, y_test) = (map['x_train'], map['y_train']), (map['x_train'], map['y_train'])

        with open('./samples/draw.json', 'r') as f:
            map_d = json.load(f)
        X_train_d, y_train_d = map_d['x_train'], map_d['y_train']
        X_train = np.concatenate([X_train, X_train_d[50:100]])
        y_train = np.concatenate([y_train, y_train_d[50:100]])
        print(y_train)
        print(y_train.shape)
        X_train = array(X_train)
        y_train = array(y_train).reshape(len(y_train),)
        print(X_train.shape)
        X_test = array(X_test)
        y_test = array(y_test).reshape(len(y_test),)

        #X_train = X_train.reshape(X_train.shape[0], 28, 28, 1)
        #X_test = X_test.reshape(X_test.shape[0], 28, 28, 1)
        # Invert
        self.X_train = X_train.astype('float32')
        self.X_test = X_test.astype('float32')

        self.X_train /= 255
        self.X_test /= 255
        # Get remainder of the division
        self.y_train_num = y_train
        self.y_test_num = y_test
        # Get array of our category
        print(y_train.shape)
        self.Y_train = np_utils.to_categorical(self.y_train_num, num_classes=3)
        print(self.Y_train.shape)
        self.Y_test = np_utils.to_categorical(self.y_test_num, num_classes=3)

    def fit(self, X: ndarray = None, y: ndarray = None):

        # Check if we have other train array
        if X:
            self.X_train = X
            self.Y_train = y
        else:
            self._get_train()

        self.model = Sequential()

        self.model.add(Convolution2D(32, (3, 3), activation='relu', input_shape=(243, 243, 1)))
        self.model.add(Convolution2D(64, (3, 3), activation='relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))

        self.model.add(Flatten())
        self.model.add(Dense(108, activation='relu'))
        self.model.add(Dropout(0.2))
        self.model.add(Dense(9, activation='relu'))
        self.model.add(Dense(3, activation='softmax'))

        self.model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

        self.model.fit(self.X_train, self.Y_train, batch_size=4, epochs=3, workers=5, verbose=1)

    def predict(self, X: ndarray = None, y: ndarray = None):
        # Check if we have other test array

        with open('./samples/xwin.json', 'r') as f:
            map = json.load(f)
        (X_train, y_train) = (map['x_train'],map['y_train'])
        X_train = array(X_train)
        y_train = array(y_train).reshape(len(y_train),)
        self.X_test = X_train
        self.Y_test = y_train
        probs = self.model.predict(self.X_test[np.newaxis,0])
        prediction = probs.argmax(axis=1)

        image = self.X_test[0]

        print(f"predicted {prediction[0]}")
        cv2.imshow("Result", image)
        cv2.waitKey(0)


        with open('./samples/0win.json', 'r') as f:
            map = json.load(f)
        (X_train, y_train) = (map['x_train'],map['y_train'])
        X_train = array(X_train)
        y_train = array(y_train).reshape(len(y_train),)
        self.X_test = X_train
        self.Y_test = y_train
        probs = self.model.predict(self.X_test[np.newaxis,0])
        prediction = probs.argmax(axis=1)

        image = self.X_test[0]

        print(f"predicted {prediction[0]}")
        cv2.imshow("Result", image)
        cv2.waitKey(0)

    def predict_multi(self, x: ndarray, show=True):
        # Get shape what we need
        x = x.astype('float32')[np.newaxis, ..., np.newaxis]

        # Get probabilities
        prob = self.model.predict(x)
        prediction = prob.argmax(axis=1)

        print(f"Actual predicted {prediction[0]}")

        # Show our predict
        if show:
            image = x.reshape((243, 243)).astype("uint8")
            cv2.imshow("Result", image)
            cv2.waitKey(0)

    def save(self, name='./check_num'):
        self.model.save(name)

    def load(self, name='./check_num'):
        try:
            self.model = load_model(name)
        except IOError as e:
            exit('Cant find this file. ')


img = cv2.imread('23.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)
img_erode = cv2.erode(thresh, np.ones((3, 3), np.uint8), iterations=0)
img_erode = cv2.resize(img_erode, (243, 243), interpolation=cv2.INTER_AREA)
#img_erode = img_erode.reshape(243, 243)
#img_arr = keras.utils.img_to_array(img).tolist()
#img_arr = array(img_arr)
#print(img_arr)
a = MnistModel()
#a.load()
a.fit()

a.predict_multi(img_erode)

# k = symbols_extract()
# print()
# a.predict_multi(array(k))